﻿using System;
namespace monis.api
{
    public enum Authorization
    {
        None,
        SendOnly,
        ReceiveOnly,
        SendAndReceive
    }
}
