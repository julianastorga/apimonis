﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class Source
    {
        [JsonProperty(PropertyName = parameters.Client)]
        public String Client { get; set; }

        [JsonProperty(PropertyName = parameters.CurrencyId)]
        public decimal CurrencyId { get; set; }

        [JsonProperty(PropertyName = parameters.Auth)]
        public Auth Auth { get; set; }
    }
}
