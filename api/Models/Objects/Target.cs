﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class Target
    {
        [JsonProperty(PropertyName = parameters.Client)]
        public String Client { get; set; }
    }
}
