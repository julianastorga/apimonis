﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class Auth
    {
        [JsonProperty(PropertyName = parameters.Password)]
        public String Password { get; set; }

        [JsonProperty(PropertyName = parameters.Pin)]
        public String Pin { get; set; }

        [JsonProperty(PropertyName = parameters.OTP)]
        public String OTP { get; set; }

        [JsonProperty(PropertyName = parameters.BingoCard)]
        public String BingoCard { get; set; }
    }
}
