﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace monis.api
{
    public class Account
    {
        [JsonProperty(PropertyName = parameters.KeyName)]
        public String KeyName { get; set; }

        [JsonProperty(PropertyName = parameters.Id)]
        public int Id { get; set; }

        [JsonProperty(PropertyName = parameters.BankAccounts)]
        public ObservableCollection<BankAccount> BankAccounts { get; set; }

        [JsonProperty(PropertyName = parameters.AccountType)]
        public Item AccountType { get; set; }
    }
}
