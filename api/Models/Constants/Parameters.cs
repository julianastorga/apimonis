﻿using System;
using System.Data;
namespace monis.api
{
    public static class parameters
    {
        public const String Id = "id";
        public const String Source = "source";
        public const String Target = "target";
        public const String Client = "client";
        public const String Password = "password";
        public const String Pin = "pin";
        public const String OTP = "otp";
        public const String BingoCard = "bingo_card";
        public const String Amount = "amount";
        public const String TransactionTypeId = "transaction_type_id";
        public const String ReferenceNumber = "reference_number";
        public const String CurrencyId = "currency_id";
        public const String Description = "description";
        public const String ChannelId = "channel_id";
        public const String DeviceId = "device_id";
        public const String Timestamp = "timestamp";
        public const String Auth = "auth";
        public const String Execution = "execution";
        public const String Data = "data";
        public const String Account = "account";
        public const String KeyName = "key_name";
        public const String BankAccounts = "bank_accounts";
        public const String AccountType = "account_type";
        public const String Number = "number";
        public const String Currency = "currency";
        public const String Subjects = "subjects";
        public const String Transaction = "transaction";

    }
}
