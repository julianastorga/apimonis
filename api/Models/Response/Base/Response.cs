﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class Response
    {
        public String AuthorizationToken { get; set; }

        [JsonProperty(PropertyName = parameters.Execution)]
        public Object Execution { get; set; }

        [JsonProperty(PropertyName = parameters.Data)]
        public Object Data { get; set; }
    }
}
