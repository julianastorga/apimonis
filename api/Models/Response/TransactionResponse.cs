﻿using System;
using Newtonsoft.Json;

namespace monis.api
{
    public class TransactionResponse : Response
    {
        [JsonProperty(PropertyName = parameters.Data)]
        public new TransactionData Data { get; set; }

    }
}
