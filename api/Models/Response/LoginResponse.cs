﻿using System;
using Newtonsoft.Json;
namespace monis.api
{
    public class LoginResponse : Response
    {
        [JsonProperty(PropertyName = parameters.Data)]
        public new LoginData Data { get; set; }
    }
}
