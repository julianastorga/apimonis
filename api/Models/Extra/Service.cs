﻿using System;
using Newtonsoft.Json;
using System.Net.Http;


namespace monis.api
{
    public class Service
    {
        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }

        [JsonProperty(PropertyName = "method")]
        private String _method { get; set; }
        public HttpMethod Method
        {
            get
            {
                switch (_method)
                {
                    case "POST":
                        return HttpMethod.Post;
                    case "GET":
                        return HttpMethod.Get;
                    case "PUT":
                        return HttpMethod.Put;
                    case "DELETE":
                        return HttpMethod.Delete;
                    default:
                        return null;
                }
            }
        }

        [JsonProperty(PropertyName = "url")]
        public String Url { get; set; }

        [JsonProperty(PropertyName = "auth")]
        public Authorization AuthorizationBehavior { get; set; }


    }
}
