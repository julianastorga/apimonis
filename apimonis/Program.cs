﻿using System;
using monis.api;
using System.Collections.Generic;
using System.Net.Http;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;

namespace apimonis
{
    class Program
    {
        public static Dictionary<String, ObservableCollection<Item>> MonisCatalogs = new Dictionary<string, ObservableCollection<Item>>();

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            String AuthorizationToken = String.Empty;
            HttpClient Client = null;

            try
            {
                /*
                 * Paso 0: Inicialización
                 * Token de Autorización
                 * Client HTTP
                 */

                AuthorizationToken = String.Empty;
                Client = new HttpClient();

                /*
                 * Paso 1: Login
                 * Se utilizan las siguientes credenciales: 
                 * 88843273 : miclave6000
                 */

                var login = RequestCreator.Make<LoginRequest>(Services.Login, Program.MakeLoginParameters());
                var Response = await Monis.CallAsync<LoginResponse>(Client, login);
                AuthorizationToken = Response.AuthorizationToken;

                /* Paso 2: Catálogos
                 * Se consumen los catálogos de monedas y tipos de transacción. 
                 */

                var all_currencies = RequestCreator.Make<Request>(Services.CatalogCurrency);
                var Currencies = (ObservableCollection<Item>)await Monis.Query<Item>(Client, all_currencies);
                Program.MonisCatalogs.Add("Currencies", Currencies);

                var all_transaction_types = RequestCreator.Make<Request>(Services.CatalogTransactionType);
                var TransactionTypes = await Monis.Query<Item>(Client, all_transaction_types);
                Program.MonisCatalogs.Add("TransactionTypes", TransactionTypes);

                /* Paso 3: Evaluar Transacción
                 * Se envían los datos a un destino SINPE Móvil
                 */

                var Colones = MonisCatalogs["Currencies"].FirstOrDefault(c => c.Key == "crc");
                var SinpeMovil = MonisCatalogs["TransactionTypes"].FirstOrDefault(t => t.Key == "sinpe_movil_transfer");
                var ColonesAccount = Response.Data.Account.BankAccounts.FirstOrDefault(ba => ba.Currency.Key == Colones.Key);
                String SM_Target = "88389250";


                var Source = Program.MakeSource(ColonesAccount.Number);
                var Target = Program.MakeTarget(SM_Target);
                var evaluate_transaction = RequestCreator.Make<TransactionRequest>(Services.EvaluateTransaction, Program.MakeEvaluateTransactionParameters(Source, Target, Colones, SinpeMovil));
                var evaluate_response = await Monis.CallAsync<TransactionResponse>(Client, evaluate_transaction, AuthToken: AuthorizationToken);


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        internal static Dictionary<String, Object> MakeLoginParameters()
        {
            try
            {
                return new Dictionary<string, object> { { parameters.Client, "88843273" }, { parameters.Password, "miclave6000" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Source MakeSource(String AccountNumber)
        {
            try
            {
                return new Source()
                {
                    Client = AccountNumber,
                    Auth = new Auth()
                    {
                        Password = "miclave6000"
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Target MakeTarget(String TargetClient)
        {
            try
            {
                return new Target()
                {
                    Client = TargetClient
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static Dictionary<String, Object> MakeEvaluateTransactionParameters(Source TransactionSource, Target TransactionTarget, Item TransactionCurrency, Item TransactionType)
        {
            try
            {
                return new Dictionary<string, object>(){
                    {parameters.Source, TransactionSource},
                    {parameters.Target, TransactionTarget},
                    {parameters.Amount, "100.00"},
                    {parameters.Description, "Demo SINPE Móvil"},
                    {parameters.CurrencyId, int.Parse(TransactionCurrency.Id)},
                    {parameters.TransactionTypeId, int.Parse(TransactionType.Id)}
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




    }
}
